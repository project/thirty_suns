30suns makes it easy to start logging service incidents on a status dashboard today.

This module was developed by Alessandro Feijó (@afeijo) https://drupal.org/user/165811 and sponsored by 30suns itself.
