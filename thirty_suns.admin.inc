<?php

/**
 * Implements admin page.
 */
function thirty_suns_admin() {
  $form['30suns_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('30suns_username', ''),
    // '#size' => 20,
    '#maxlength' => 30,
    '#description' => t('Enter your 30suns username'),
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => 'Please enter your 30suns username',
    ),
  );

  $register = l('Sign up for a 30suns account', 'https://30suns.com/register/', array( 'attributes' => array('target' => '_blank', ), ));

  $variables=array();
  $variables['title']=t('30suns makes it easy to start logging service incidents on a status dashboard today.').'<br>'.
    t('To get started:');
  $variables['type']='ol';
  $variables['attributes']=array();
  $variables['items'] = array(
    t('Enter your username above and click "Save configuration"'),
    t('Create a status page'),
    t('Go to the "Blocks" settings page and position your 30suns block on your status page.',
    array('!register_link' => $register, )),
  );
  $form['info']['#markup'] = theme_item_list($variables);

  return system_settings_form($form);
}
